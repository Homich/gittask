
public with sharing class StringTasks {

    /**
 * Returns the result of concatenation of two strings.
 *
 * @param {string} value1
 * @param {string} value2
 * @return {string}
 *
 * @example
 *   'aa', 'bb' => 'aabb'
 *   'aa',''    => 'aa'
 *   '',  'bb'  => 'bb'
 */
    public static String concatenateStrings(String value1, String value2) {
        return value1 + value2;
    }

    /**
 * Returns the length of given string.
 *
 * @param {string} value
 * @return {number}
 *
 * @example
 *   'aaaaa' => 5
 *   'b'     => 1
 *   ''      => 0
 */
    public static Integer getStringLength(String value) {
        return value.length();
    }

    /**
 * Returns the result of string template and given parameters firstName and lastName.
 * Please do not use concatenation, use template string :
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/template_strings
 *
 * @param {string} firstName
 * @param {string} lastName
 * @return {string}
 *
 * @example
 *   'John','Doe'      => 'Hello, John Doe!'
 *   'Chuck','Norris'  => 'Hello, Chuck Norris!'
 */
    public static String getStringFromTemplate(String firstName, String lastName) {
        return 'Hello, ' + firstName + ' ' + lastName + '!';
    }

    /**
 * Extracts a name from template string 'Hello, First_Name Last_Name!'.
 *
 * @param {string} value
 * @return {string}
 *
 * @example
 *   'Hello, John Doe!' => 'John Doe'
 *   'Hello, Chuck Norris!' => 'Chuck Norris'
 */
    public static String extractNameFromTemplate(String value) {
        value = value.remove('Hello, ');
        value = value.remove('!');
        return value;
    }

    /**
 * Returns a first char of the given string.
 *
 * @param {string} value
 * @return {string}
 *
 * @example
 *   'John Doe'  => 'J'
 *   'cat'       => 'c'
 */
    public static String getFirstChar(String value) {
        List<Integer> arr = new Integer[]{value.charAt(0)};
        return String.fromCharArray(arr);
    }

    /**
 * Removes a leading and trailing whitespace characters from string.
 *
 * @param {string} value
 * @return {string}
 *
 * @example
 *   '  Abracadabra'    => 'Abracadabra'
 *   'cat'              => 'cat'
 *   '\tHello, World! ' => 'Hello, World!'
 */
    public static String removeLeadingAndTrailingWhitespaces(String value) {

        List<Integer> arr = value.getChars();

        if ( arr.get(0) == 9){
            do{ arr.remove(0); }while ( arr.get(0) == 9 );
        }
        if ( arr.get(0) == 32){
            do{ arr.remove(0); }while ( arr.get(0) == 32 );
        }
        if ( arr.get(arr.size()-1) == 9){
            do{ arr.remove(arr.size()-1); }while ( arr.get(arr.size()-1) == 9 );
        }
        if ( arr.get(arr.size()-1) == 32){
            do{ arr.remove(arr.size()-1); }while ( arr.get(arr.size()-1) == 32 );
        }

        return String.fromCharArray(arr);
    }

    /**
 * Returns a string that repeated the specified number of times.
 *
 * @param {string} value
 * @param {string} count
 * @return {string}
 *
 * @example
 *   'A', 5  => 'AAAAA'
 *   'cat', 3 => 'catcatcat'
 */
    public static String repeatString(String value, Integer count) {
        return value.repeat(count);
    }

    /**
 * Remove the first occurrence of string inside another string
 *
 * @param {string} str
 * @param {string} value
 * @return {string}
 *
 * @example
 *   'To be or not to be', 'not'  => 'To be or to be'
 *   'I like legends', 'end' => 'I like legs',
 *   'ABABAB','BA' => 'ABAB'
 */
    public static String removeFirstOccurrences(String str, String value) {
        Integer poinerEnter = str.indexOf(value);
        List<Integer> arr1 = str.getChars();

        if ( poinerEnter != -1 ) {
            for ( Integer i = poinerEnter ; i < poinerEnter+value.length(); i++ ){
                arr1.remove(poinerEnter);
            }
        }

        return String.fromCharArray(arr1);
    }

    /**
 * Remove the first and last angle brackets from tag string
 *
 * @param {string} str
 * @return {string}
 *
 * @example
 *   '<div>' => 'div'
 *   '<span>' => 'span'
 *   '<a>' => 'a'
 */
    public static String unbracketTag(String str) {
        str = StringTasks.removeFirstOccurrences(str,'<');
        str = str.reverse();
        str = StringTasks.removeFirstOccurrences(str,'>');
        return str.reverse();
    }

    /**
 * Converts all characters of the specified string into the upper case
 *
 * @param {string} str
 * @return {string}
 *
 * @example
 *   'Thunderstruck' => 'THUNDERSTRUCK'
 *  'abcdefghijklmnopqrstuvwxyz' => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
 */
    public static String convertToUpperCase(String str) {
        return str.toUpperCase();
    }

    /**
 * Extracts e-mails from single string with e-mails list delimeted by semicolons
 *
 * @param {string} str
 * @return {array}
 *
 * @example
 *   'angus.young@gmail.com;brian.johnson@hotmail.com;bon.scott@yahoo.com' => ['angus.young@gmail.com', 'brian.johnson@hotmail.com', 'bon.scott@yahoo.com']
 *   'info@gmail.com' => ['info@gmail.com']
 */
    public static List<String> extractEmails(String str) {
        return str.split(';');
    }

    /**
 * Returns the string representation of rectangle with specified width and height
 * using pseudograhic chars
 *
 * @param {number} width
 * @param {number} height
 * @return {string}
 *
 * @example
 *
 *            '┌────┐\n'+
 *  (6,4) =>  '│    │\n'+
 *            '│    │\n'+
 *            '└────┘\n'
 *
 *  (2,2) =>  '┌┐\n'+
 *            '└┘\n'
 *
 *             '┌──────────┐\n'+
 *  (12,3) =>  '│          │\n'+
 *             '└──────────┘\n'
 *
 */
    public static String getRectangleString(Integer width, Integer height) {

        String result ='';


        for ( Integer i = 0; i < height; i++ ){

            for ( Integer j = 0; j < width; j++ ){

                if ( ( i == 0 ) || ( i == height-1 ) || ( j == 0 ) || ( j == width-1 ) ) {
                    if ( (i == 0) && (j == 0) ){ result += '┌'; }
                    if ( (i == 0) && (j == width-1) ){ result += '┐'; }
                    if ( (i == height-1) && (j == 0) ){ result += '└'; }
                    if ( (i == height-1) && (j == width-1) ){ result += '┘'; }

                    if ( (i == 0) && (j != width-1) && (j != 0) || (i == height-1) && (j != width-1) && (j != 0) ) { result += '─'; }
                    if ( (j == 0) && (i != height-1) && (i != 0) || (j == width-1) && (i != height-1) && (i != 0) ) { result += '│'; }
                }else {
                    result += ' ';
                }


            }
            result += '\n';

        }

        return result;
    }

    /**
 * Returns playid card id.
 *
 * Playing cards inittial deck inclides the cards in the following order:
 *
 *  'A♣','2♣','3♣','4♣','5♣','6♣','7♣','8♣','9♣','10♣','J♣','Q♣','K♣',
 *  'A♦','2♦','3♦','4♦','5♦','6♦','7♦','8♦','9♦','10♦','J♦','Q♦','K♦',
 *  'A♥','2♥','3♥','4♥','5♥','6♥','7♥','8♥','9♥','10♥','J♥','Q♥','K♥',
 *  'A♠','2♠','3♠','4♠','5♠','6♠','7♠','8♠','9♠','10♠','J♠','Q♠','K♠'
 *
 * (see https://en.wikipedia.org/wiki/Standard_52-card_deck)
 * Function returns the zero-based index of specified card in the initial deck above.
 *
 * @param {string} value
 * @return {number}
 *
 * @example
 *   'A♣' => 0
 *   '2♣' => 1
 *   '3♣' => 2
 *     ...
 *   'Q♠' => 50
 *   'K♠' => 51
 */
    public static Integer getCardId(String value) {
        List<String> str = new List<String>{'A♣','2♣','3♣','4♣','5♣','6♣','7♣','8♣','9♣','10♣','J♣','Q♣','K♣',
                                            'A♦','2♦','3♦','4♦','5♦','6♦','7♦','8♦','9♦','10♦','J♦','Q♦','K♦',
                                            'A♥','2♥','3♥','4♥','5♥','6♥','7♥','8♥','9♥','10♥','J♥','Q♥','K♥',
                                            'A♠','2♠','3♠','4♠','5♠','6♠','7♠','8♠','9♠','10♠','J♠','Q♠','K♠'};
        Integer num = -1;
        for ( String st : str ){
            num++;
            if ( st.equals(value) ){
                return num;
            }

        }
        return null;
    }

}