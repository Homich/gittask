public class LoopsTasks {

  /**
   * Returns the 'Fizz','Buzz' or an original number as String using the following rules:
   * 1) return original number as String
   * 2) but if number multiples of three return 'Fizz'
   * 3) for the multiples of five return 'Buzz'
   * 4) for numbers which are multiples of both three and five return 'FizzBuzz'
   *
   * @param {number} num
   * @return {any}
   *
   * @example
   *   2 =>  '2'
   *   3 => 'Fizz'
   *   5 => 'Buzz'
   *   4 => '4'
   *  15 => 'FizzBuzz'
   *  20 => 'Buzz'
   *  21 => 'Fizz'
   *
   */
  public static String getFizzBuzz(Integer num) {

    if ( ( num/3*3 == num ) && ( num/5*5 == num ) ){
      return 'FizzBuzz';
    }else if ( ( num/3*3 == num ) || ( num/5*5 == num ) ) {
      if ( num/3*3 == num ) {
        return  'Fizz';
      } else return 'Buzz';
    }else return ''+num;
  }

  /**
   * Returns the factorial of the specified integer n.
   *
   * @param {number} n
   * @return {number}
   *
   * @example:
   *   1  => 1
   *   5  => 120
   *   10 => 3628800
   */
  public static Integer getFactorial(Integer num) {
    Integer result = 1;
    if ( num == 0 || num == 1 ){ return result;}

    for ( Integer  i = 2; i <= num; i++ ){
      result *= i;
    }
    return result;
  }

  /**
   * Returns the sum of integer numbers between n1 and n2 (inclusive).
   *
   * @param {number} n1
   * @param {number} n2
   * @return {number}
   *
   * @example:
   *   1,2   =>  3  ( = 1+2 )
   *   5,10  =>  45 ( = 5+6+7+8+9+10 )
   *   -1,1  =>  0  ( = -1 + 0 + 1 )
   */
  public static Integer getSumBetweenNumbers(Integer num1, Integer num2) {
    Integer result = 0;
    for ( Integer i = num1; i <= num2; i++ ){
      result += i;
    }
    return result;
  }

  /**
   * Returns true, if a triangle can be built with the specified sides a,b,c and false in any other ways.
   *
   * @param {number} a
   * @param {number} b
   * @param {number} c
   * @return {bool}
   *
   * @example:
   *   1,2,3    =>  false
   *   3,4,5    =>  true
   *   10,1,1   =>  false
   *   10,10,10 =>  true
   */
  public static Boolean isTriangle(Integer a, Integer b, Integer c) {
    if ( ( a + b > c ) && ( a + c > b ) && ( b + c > a ) ) {
      return true;
    }else return false;
  }

  /**
   * Returns true, if two specified axis-aligned rectangles overlap, otherwise false.
   * Each rectangle representing by Map<String, Integer> 
   *  {
   *     top: 5,
   *     left: 5,
   *     width: 20,
   *     height: 10
   *  }
   * 
   *  (5;5)
   *     -------------  
   *     |           | 
   *     |           |  height = 10
   *     ------------- 
   *        width=20    
   * 
   * NOTE: Please use canvas coordinate space (https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Drawing_shapes#The_grid),
   * it differs from Cartesian coordinate system.
   * 
   * @param {Map<String, Integer>} rect1
   * @param {Map<String, Integer>} rect2
   * @return {bool}
   *
   * @example:
   *   { top: 0, left: 0, width: 10, height: 10 },
   *   { top: 5, left: 5, width: 20, height: 20 }    =>  true
   * 
   *   { top: 0, left: 0, width: 10, height: 10 },
   *   { top:20, left:20, width: 20, height: 20 }    =>  false
   *  
   */
  public static Boolean doRectanglesOverlap(Map<String, Integer> rect1, Map<String, Integer> rect2) {

    Integer top1 = rect1.get('top');
    Integer top2 = rect2.get('top');
    Integer left1 = rect1.get('left');
    Integer left2 = rect2.get('left');
    Integer width1 = rect1.get('width');
    Integer width2 = rect2.get('width');
    Integer height1 = rect1.get('height');
    Integer height2 = rect2.get('height');

    if ( ( top1+height1 <= top2 ) || ( top2+height2 <= top1 ) || ( left1+width1 <= left2 ) || ( left1 >= left2+width2 ) ){
      return false;
    } else return true;
  }

  /**
   * Returns true, if point lies inside the circle, otherwise false.
   * Circle is:
   *     Center Map<String, Double>: {
   *       x: 5,       
   *       y: 5
   *     },        
   *     Radius Integer: 20
   * Point is Map<String, Double> 
   *  {
   *     x: 5,
   *     y: 5
   *  }
   * 
   * @param {Map<String, Double>} center
   * @param {Integer} raduis
   * @param {Map<String, Double>} point
   * @return {bool}
   *
   * @example:
   *   center: { x:0, y:0 }, radius: 10, point: { x:0, y:0 }     => true
   *   center: { x:0, y:0 }, radius:10,  point: { x:10, y:10 }   => false
   *   
   */
  public static Boolean isInsideCircle(Map<String, Double> center, Integer radius, Map<String, Double> point) {

    if ( Math.sqrt( Math.pow( point.get('x')-center.get('x') , 2 ) + Math.pow( point.get('y')-center.get('y') , 2 ) ) < radius ){
      return true;
    } else return false;
  }

  /**
   * Returns the first non repeated char in the specified strings otherwise returns null.
   *
   * @param {string} str
   * @return {string}
   *
   * @example:
   *   'The quick brown fox jumps over the lazy dog' => 'T'
   *   'abracadabra'  => 'c'
   *   'entente' => null
   */
  public static String findFirstSingleChar(String str) {

    List<Integer> charArray = str.getChars();
    List<Integer> result = new List<Integer>();
    Integer counter = 0;

    for ( Integer i = 0; i < charArray.size(); i++ ){
      counter = 0;
      for ( Integer j = 0; j < charArray.size(); j++ ){
        if ( i != j ) {
          if ( charArray.get(j) == charArray.get(i) ) {
            counter++;
            break;
          }
        }
      }
      if ( counter == 0 ) {
        result.add(charArray.get(i));
        return String.fromCharArray(result);
      }
    }

    return null;
  }

  /**
   * Returns the string representation of math interval, specified by two points and include / exclude flags.
   * See the details: https://en.wikipedia.org/wiki/Interval_(mathematics)
   *
   * Please take attention, that the smaller number should be the first in the notation
   *
   * @param {number} a
   * @param {number} b
   * @param {bool} isStartIncluded
   * @param {bool} isEndIncluded
   * @return {string}
   *
   * @example
   *   0, 1, true, true   => '[0, 1]'
   *   0, 1, true, false  => '[0, 1)'
   *   0, 1, false, true  => '(0, 1]'
   *   0, 1, false, false => '(0, 1)'
   * Smaller number has to be first :
   *   5, 3, true, true   => '[3, 5]'
   *
   */
  public static String getIntervalString(Integer a, Integer b, Boolean isStartIncluded, Boolean isEndIncluded) {
    String result = '';
    if ( isStartIncluded ){
      result += '[';
    } else {
      result += '(';
    }
    if ( a < b ){
      result += a +', '+b;
    } else {
      result += b +', '+a;
    }
    if ( isEndIncluded ){
      result += ']';
    } else {
      result += ')';
    }
    return result;
  }

  /**
   * Reverse the specified string (put all chars in reverse order)
   *
   * @param {string} str
   * @return {string}
   *
   * @example:
   * 'The quick brown fox jumps over the lazy dog' => 'god yzal eht revo spmuj xof nworb kciuq ehT'
   * 'abracadabra' => 'arbadacarba'
   * 'rotator' => 'rotator'
   * 'noon' => 'noon'
   */
  public static String reverseString(String str) {
    return str.reverse();
  }

  /**
   * Reverse the specified integer number (put all digits in reverse order)
   *
   * @param {number} num
   * @return {number}
   *
   * @example:
   *   12345 => 54321
   *   1111  => 1111
   *   87354 => 45378
   *   34143 => 34143
   */
  public static Integer reverseInteger(Integer num) {
    String str = ''+num;
    return Integer.valueOf(str.reverse());
  }

  /**
   * Returns the digital root of integer:
   *   step1 : find sum of all digits
   *   step2 : if sum > 9 then goto step1 otherwise return the sum
   *
   * @param {number} n
   * @return {number}
   *
   * @example:
   *   12345 ( 1+2+3+4+5 = 15, 1+5 = 6) => 6
   *   23456 ( 2+3+4+5+6 = 20, 2+0 = 2) => 2
   *   10000 ( 1+0+0+0+0 = 1 ) => 1
   *   165536 (1+6+5+5+3+6 = 26,  2+6 = 8) => 8
   */
  public static Integer getDigitalRoot(Integer num) {

    Integer sumOfDigits = 0;

    while ( num > 0 ){
      Integer constNum = num;
      sumOfDigits += constNum - num/10*10 ;
      num /= 10;
    }
    if ( sumOfDigits > 9 ){
      sumOfDigits = LoopsTasks.getDigitalRoot(sumOfDigits);
    }
    return sumOfDigits;
  }

  /**
   * Returns true if the specified string has the balanced brackets and false otherwise.
   * Balanced means that is, whether it consists entirely of pairs of opening/closing brackets
   * (in that order), none of which mis-nest.
   * Brackets include [],(),{},<>
   *
   * @param {string} str
   * @return {boolean}
   *
   * @example:
   *   '' => true
   *   '[]'  => true
   *   '{}'  => true
   *   '()   => true
   *   '[[]' => false
   *   ']['  => false
   *   '[[][][[]]]' => true
   *   '[[][]][' => false
   *   '{)' = false
   *   '{[(<{[]}>)]}' = true 
   */
  public static Boolean isBracketsBalanced(String str) {
    List<Integer> charArray = str.getChars();
    Integer counter;

    while (charArray.size() > 0){
      counter = 0;
      for ( Integer i = 0; i < charArray.size()-1; i++ ){
        if ( (0 > charArray.get(i) - charArray.get(i+1)) && ( (-3) < charArray.get(i) - charArray.get(i+1) ) ) {
          counter++;
          charArray.remove(i);
          charArray.remove(i);
          --i;
        }
      }
      if (counter == 0) {
        return false;
      }
    }
    return true;
  }

  /**
   * Returns the human readable string of time period specified by the start and end time.
   * The result string should be constrcuted using the folliwing rules:
   *
   * ---------------------------------------------------------------------
   *   Difference                 |  Result
   * ---------------------------------------------------------------------
   *    0 to 45 seconds           |  a few seconds ago
   *   45 to 90 seconds           |  a minute ago
   *   90 seconds to 45 minutes   |  2 minutes ago ... 45 minutes ago
   *   45 to 90 minutes           |  an hour ago
   *  90 minutes to 22 hours      |  2 hours ago ... 22 hours ago
   *  22 to 36 hours              |  a day ago
   *  36 hours to 25 days         |  2 days ago ... 25 days ago
   *  25 to 45 days               |  a month ago
   *  45 to 345 days              |  2 months ago ... 11 months ago
   *  345 to 545 days (1.5 years) |  a year ago
   *  546 days+                   |  2 years ago ... 20 years ago
   * ---------------------------------------------------------------------
   *
   * @param {DateTime} startDate
   * @param {DateTime} endDate
   * @return {string}
   *
   * @example
   *   DateTime('2000-01-01 01:00:00'), DateTime('2000-01-01 01:00:01')  => 'a few seconds ago'
   *   DateTime('2000-01-01 01:00:00'), DateTime('2000-01-01 01:00:05')  => '5 minutes ago'
   *   DateTime('2000-01-01 01:00:00'), DateTime('2000-01-02 03:00:05')  => 'a day ago'
   *   DateTime('2000-01-01 01:00:00'), DateTime('2015-01-02 03:00:05')  => '15 years ago'
   *
   */
  public static String timespanToHumanString(DateTime startDate, DateTime endDate) {

    Long amoutOfSeconds = ((endDate.getTime() - startDate.getTime()) / 1000);



      if ( (amoutOfSeconds > 0) && (amoutOfSeconds <= 45) ) {
          return 'a few seconds ago';
      } else if ( (amoutOfSeconds > 45) && (amoutOfSeconds <= 90) ) {
          return 'a minute ago';
      } else if ( (amoutOfSeconds > 90) && (amoutOfSeconds <= 2700) ) { // minutes
          if ( amoutOfSeconds/60*60 == amoutOfSeconds ) {
              return amoutOfSeconds/60+' minutes ago';
          }else {
              if ( (60 - (amoutOfSeconds - amoutOfSeconds/60*60)) >= 30 ) { return (amoutOfSeconds/60)+' minutes ago'; } else {return (amoutOfSeconds/60+1)+' minutes ago';}
          }
      } else if ( (amoutOfSeconds > 2700) && (amoutOfSeconds <= 5400) ) {
          return 'an hour ago';
      } else if ( (amoutOfSeconds > 5400) && (amoutOfSeconds <= 79200) ) { //hours
          if ( amoutOfSeconds/3600*3600 == amoutOfSeconds ) {
              return amoutOfSeconds/3600+' hours ago';
          }else {
              if ( (3600 - (amoutOfSeconds - amoutOfSeconds/3600*3600)) >= 1800 ) { return (amoutOfSeconds/3600)+' hours ago';} else { return (amoutOfSeconds/3600+1)+' hours ago';}
          }
      } else if ( (amoutOfSeconds > 79200) && (amoutOfSeconds <= 129600) ) {
          return 'a day ago';
      } else if ( (amoutOfSeconds > 12960) && (amoutOfSeconds <= 25*86400) ) { //days
          if ( amoutOfSeconds/86400*86400 == amoutOfSeconds ) {
              return amoutOfSeconds/86400+' days ago';
          }else {
              if ( (86400 - (amoutOfSeconds - amoutOfSeconds/86400*86400)) >= 43200 ) {return (amoutOfSeconds/86400)+' days ago';} else { return (amoutOfSeconds/86400+1)+' days ago';}
          }
      } else if ( (amoutOfSeconds > 25*86400) && (amoutOfSeconds <= 45*86400) ) {
          return 'a month ago';
      } else if ((endDate.year() == startDate.year()) && (endDate.month() - startDate.month() <= 11)){
          if ( (endDate.month() - startDate.month() == 11) && ( (endDate.day() - startDate.day()) >= 11 )) {
              return 'a year ago';
          } else
          if ( (endDate.day() - startDate.day()) < 11 ) {
              return endDate.month() - startDate.month() + ' months ago';
          } else { return endDate.month() - startDate.month() + 1 + ' months ago'; }
      } else {
          if ( (endDate.year() - startDate.year()) <= 1 ) {
              return 'a year ago';
          } else { return (endDate.year() - startDate.year())+' years ago';}
      }

  }
}